# neofetch _nicer gentoo logo_

Stock neofetch from [https://github.com/dylanaraps/neofetch](https://github.com/dylanaraps/neofetch) with a nicer logo for Gentoo Linux.

Sample output:

![gentoo-logo.png](gentoo-logo.png)
